package ee.mkalmo;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.regex.Pattern;

import javax.xml.parsers.*;

import org.apache.ivy.ant.IvyCacheTask;
import org.apache.ivy.core.report.ArtifactDownloadReport;
import org.apache.ivy.util.Message;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.util.DOMElementWriter;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import ee.mkalmo.util.Util;

/**
 * An Ant task that updates a project's Eclipse .classpath file according
 * to the result of <code>ivy:resolve</code>. Supports
 * attaching sources jars to entries if any are available.
 */
public class EclipseClasspath extends IvyCacheTask {
    private static final String ATTR_IVYGEN = "ivygen";
    private static final String TAG_CLASSPATH_ENTRY = "classpathentry";

    private final String SOURCE_TYPE = "source";
    private final String CLASSPATH_FILE_NAME = ".classpath";

    @Override
    public void doExecute() throws BuildException {

        prepareAndCheck();

        String original = Util.readFile(getClassPathFile());

        String modified = addEntriesFromMap(original, createEntryMap());

        writeThroughTemp(modified, getClassPathFile());
    }

    public String addEntriesFromMap(String xmlString, EntryMap map) {

        Element root = loadXml(xmlString);

        removeOldEntries(root);

        insertNewEntries(map, root);

        return asString(root);
    }

    private File getClassPathFile() {
        return new File(getProject().getBaseDir(), CLASSPATH_FILE_NAME);
    }

    private EntryMap createEntryMap() {
        EntryMap map = new EntryMap();
        for (ArtifactDownloadReport each : getArtifactReports()) {
            String artifactName = each.getArtifact().getName();
            String path = each.getLocalFile().getAbsolutePath();
            if (each.getType().equals(SOURCE_TYPE)) {
                map.get(artifactName).src = path;
            } else {
                map.get(artifactName).bin = path;
            }

            Message.verbose("Artifact " + artifactName + ": " + path);
        }

        return map;
    }

    private void removeOldEntries(Element root) {
        List<Node> toBeRemoved = new ArrayList<Node>();
        Node runner = root.getFirstChild();

        do {

            if (isGenerated(runner) || isEmpty(runner)) {
                toBeRemoved.add(runner);
            }

            runner = runner.getNextSibling();

        } while (runner != null);

        for (Node each : toBeRemoved) {
            root.removeChild(each);
        }
    }

    private String asString(Element root) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        DOMElementWriter writer = new DOMElementWriter();
        pw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        try {
            writer.write(root, pw, 0, "\t");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sw.toString();
    }

    private void writeThroughTemp(String content, File outFile) {
        File tempFile = null;
        try {
            tempFile = File.createTempFile("ivygen-", ".tmp");

            Util.writeFile(tempFile, content);

            Files.move(tempFile.toPath(), outFile.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);

        } catch (Exception ex) {
            throw new BuildException("Unable to generate Eclipse classpath:", ex);
        } finally {
            tempFile.delete(); // if move failed
        }
    }

    private Element loadXml(String inputString) {

        try {
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(inputString));
            return builder.parse(is).getDocumentElement();
        } catch (Exception e) {
            throw new BuildException(e);
        }
    }

    private void insertNewEntries(EntryMap map, Element root) {
        for (String key : map.keySet()) {
            Element newEntry = root.getOwnerDocument()
                    .createElement(TAG_CLASSPATH_ENTRY);
            newEntry.setAttribute(ATTR_IVYGEN, "true");
            newEntry.setAttribute("kind", "lib");
            newEntry.setAttribute("path", map.get(key).bin);
            if (map.get(key).src != null) {
                newEntry.setAttribute("sourcepath", map.get(key).src);
            }
            root.appendChild(newEntry);
        }
    }

    private boolean isEmpty(Node node) {
        return node instanceof Text && Pattern.matches("\\s+", node.getTextContent());
    }

    private boolean isGenerated(Node node) {
        return node instanceof Element
            && ((Element) node).getTagName().equals(TAG_CLASSPATH_ENTRY)
            && Boolean.parseBoolean(((Element)node).getAttribute(ATTR_IVYGEN));
    }

    @Override
    protected List<ArtifactDownloadReport> getArtifactReports() {
        try {
            return super.getArtifactReports();
        } catch (Exception e) {
            throw new BuildException(e);
        }
    }

}