package ee.mkalmo;

import java.util.HashMap;

public class EntryMap extends HashMap<String, LibEntry> {
    private static final long serialVersionUID = 1L;

    @Override
    public LibEntry get(Object key) {
        LibEntry entry = super.get(key);
        if (entry == null) {
            super.put((String) key, new LibEntry());
        }
        return super.get(key);
    }
}
