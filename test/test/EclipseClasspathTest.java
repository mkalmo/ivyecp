package test;

import java.io.File;

import org.apache.tools.ant.Project;
import org.junit.Test;

import ee.mkalmo.EclipseClasspath;
import ee.mkalmo.EntryMap;
import ee.mkalmo.util.Util;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class EclipseClasspathTest {

    @Test
    public void addsEntriesFromMap() {
        EntryMap map = new EntryMap();
        map.get("junit").bin = "junit.bin.jar";
        map.get("junit").src = "junit.src.jar";

        String output = createTask().addEntriesFromMap(
                readFromFile("classpath.xml"), map);

        assertThat(output, not(containsString("toBeRemoved.jar")));
        assertThat(output, containsString("path=\"junit.bin.jar\""));
        assertThat(output, containsString("sourcepath=\"junit.src.jar\""));
    }

    private EclipseClasspath createTask() {
        EclipseClasspath task = new EclipseClasspath();
        task.setFile(new File("ivy/ivy.xml"));
        task.setConf("default");
        task.setProject(new Project());
        return task;
    }

    public String readFromFile(String fileName) {
        String filePath = getClass().getResource(fileName).getFile();
        return Util.readFile(new File(filePath));
    }
}
